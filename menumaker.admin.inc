<?php
// $Id$

/**
 * @file
 * Menumaker administration and module settings UI.
 * NOTE: This does not touch the menu_router table (and should not!)
 */

/**
 * Form builder for administering agaricgenartsproductview.
 */
function menumaker_rebuild_menus_form($form_state) {
  $form = array();
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild automatic menus'),
  );
  return $form;
}

/**
 * Handles submission of menumaker_rebuild_menus_form. 
 */
function menumaker_rebuild_menus_form_submit($form, $form_state) {
  menumaker_build_menu_links();
}

/**
 * Implement hook_menumaker_operations().
 */
function menumaker_menumaker_operations() {
  $operations = array(
/*
    'enable' => array(
      'label' => t('enable'),
      'callback' => 'node_mass_update',
      'callback arguments' => array('updates' => array('status' => 1)),
    ),
    'disable' => array(
      'label' => t('Disable'),
      'callback' => 'node_mass_update',
      'callback arguments' => array('updates' => array('status' => 0)),
    ),
*/
    'delete' => array(
      'label' => t('Delete'),
      'callback' => NULL,
    ),
  );
  return $operations;
}

/**
 * @TODO see about bringing back all this filtering stuff from node.admin.inc
 * List node administration filters that can be applied.
 * Build query for node administration filters based on session.
 * Return form for node administration filters.
 * Theme node administration filter form.
 * Theme node administration filter selector.
 * Process result from node administration filter form.
*/

/**
 * Make mass update of menu link items, changing all items in the $items array
 * to update them with the field values in $updates.
 *
 * IMPORTANT NOTE: This function is intended to work when called
 * from a form submit handler. Calling it outside of the form submission
 * process may not work correctly.
 *
 * @param array $items
 *   Array of menu link item mlids to update.
 * @param array $updates
 *   Array of key/value pairs with menu link field names and the
 *   value to update that field to.
 */
function menumanker_mass_update($items, $updates) {
  // We use batch processing to prevent timeout when updating a large number
  // of menu link items.
  if (count($items) > 10) {
    $batch = array(
      'operations' => array(
        array('_menumaker_mass_update_batch_process', array($items, $updates))
      ),
      'finished' => '_menumaker_mass_update_batch_finished',
      'title' => t('Processing'),
      // We use a single multi-pass operation, so the default
      // 'Remaining x of y operations' message will be confusing here.
      'progress_message' => '',
      'error_message' => t('The update has encountered an error.'),
      // The operations do not live in the .module file, so we need to
      // tell the batch engine which file to load before calling them.
      // @TODO change to menumaker
      'file' => drupal_get_path('module', 'agaricgenartsproductview') .'/menumaker.admin.inc',
    );
    batch_set($batch);
  }
  else {
    foreach ($items as $mlid) {
      _menumaker_mass_update_helper($mlid, $updates);
    }
    drupal_set_message(t('The update has been performed.'));
  }
}

/**
 * Menumaker Mass Update - helper function.
 */
function _menumaker_mass_update_helper($mlid, $updates) {
  $item = menumaker_find_menu_link(array('mlid' => $mlid));
  foreach ($updates as $name => $value) {
    $item[$name] = $value;
  }
  $mlid = menu_link_save($item);
  return $item;  // The mlid is just that, nothing else, return the real item.
}

/**
 * Menumaker Mass Update Batch operation
 */
function _menumaker_mass_update_batch_process($items, $updates, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($items);
    $context['sandbox']['items'] = $items;
  }

  // Process menu link items by groups of 5.
  $count = min(5, count($context['sandbox']['items']));
  for ($i = 1; $i <= $count; $i++) {
    // For each mlid, load the menu link item, reset the values, and save it.
    $mlid = array_shift($context['sandbox']['items']);
    $item = _menumaker_mass_update_helper($mlid, $updates);

    // Store result for post-processing in the finished callback.
    $context['results'][] = l($item['link_title'], $item['link_path']);

    // Update our progress information.
    $context['sandbox']['progress']++;
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Menu Mass Update Batch 'finished' callback.
 */
function _menumaker_mass_update_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The update has been performed.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
    $message = format_plural(count($results), '1 item successfully processed:', '@count items successfully processed:');
    $message .= theme('item_list', $results);
    drupal_set_message($message);
  }
}

/**
 * Menu callback: content administration.
 */
function menumaker_bulk_edit_items($form_state, $broken = FALSE) {
  if ($broken) {
    $broken = TRUE;
  }
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return menumaker_multiple_delete_confirm($form_state, array_filter($form_state['values']['items']));
  }
  $form = array(); // This is where it started with filter form in node.admin.
  $form['admin'] = menumaker_bulk_edit_items_form($broken);

  return $form;
}

/**
 * Form builder: Builds the bulk menu editor administration overview.
 */
function menumaker_bulk_edit_items_form($broken = FALSE) {
  // (removed filter stuff here and below)
  // Harcode primary links for now
  $result = pager_query('SELECT * FROM {menu_links} WHERE menu_name = "primary-links" ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC', 50, 0, NULL);

  if ($broken) {
    global $user;
    if ($user->uid != 1) {
      $form['broken'] = array('#value' => "<p>You tried to view only broken links but we cannot let you because menu_valid_path() also checks if a user has access to the path, so valid paths may be reported as broken.  Please try again as user #1.</p>");
      $broken = TRUE;
      continue;
    }
    // Using a pager for broken links is not feasible because we filter out the
    // good links after the SQL is done.
    $result = db_query('SELECT * FROM {menu_links} WHERE menu_name = "primary-links" ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC', 50, 0, NULL);
    $form['broken'] = array('#value' => "<p>Congratulations, you have found the secret form to display <strong>only broken menu links</strong>.  It is probably fairly safe to delete with abandon.</p>.");
  }

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $options = array();
  foreach (module_invoke_all('menumaker_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('menumaker_bulk_edit_items_form_submit'),
  );

  $destination = drupal_get_destination();
  $items = array();
  while ($item = db_fetch_object($result)) {
    if ($broken && menu_valid_path((array)$item)) {
      continue;
    }
    $items[$item->mlid] = '';
    $form['title'][$item->mlid] = array('#value' => l($item->link_title, $item->link_path, $options));
    $form['menu_name'][$item->mlid] =  array('#value' => $item->menu_name);
    $form['router_path'][$item->mlid] = array('#value' => $item->router_path);
    $form['module'][$item->mlid] =  array('#value' => $item->module);
    $form['operations'][$item->mlid] = array('#value' => l(t('edit'), 'admin/build/menu/item/' . $item->mlid . '/edit', array('query' => $destination)));
  }
  $form['items'] = array('#type' => 'checkboxes', '#options' => $items);
  if (!$broken) {
    $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  }
  $form['#theme'] = 'menumaker_bulk_edit_items_form';
  return $form;
}

/**
 * Validate menumaker_bulk_edit_items_form form submissions.
 * 
 * Check if any nodes have been selected to perform the chosen
 * 'Update option' on.
 */
function menumaker_bulk_edit_items_form_validate($form, &$form_state) {
  $items = array_filter($form_state['values']['items']);
  if (count($items) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Process menumaker_bulk_edit_items_form form submissions.
 * 
 * Execute the chosen 'Update option' on the selected nodes.
 */
function menumaker_bulk_edit_items_form_submit($form, &$form_state) {
  $operations = module_invoke_all('menumaker_operations');
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked nodes
  $items = array_filter($form_state['values']['items']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($items), $operation['callback arguments']);
    }
    else {
      $args = array($items);
    }
    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step.  For example, to
    // show the confirmation form for the deletion of nodes.
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Theme node administration overview.
 *
 * @ingroup themeable
 */
function theme_menumaker_bulk_edit_items_form($form) {
  // If there are rows in this form, then $form['title'] contains a list of
  // the title form elements.
  $has_posts = isset($form['title']) && is_array($form['title']);
  $select_header = $has_posts ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Title'), t('Menu'), t('Router path'), t('Module'));
  $header[] = t('Operations');
  $output = '';

  $output .= drupal_render($form['options']);
  if ($has_posts) {
    foreach (element_children($form['title']) as $key) {
      $row = array();
      $row[] = drupal_render($form['items'][$key]);
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['menu_name'][$key]);
      $row[] = drupal_render($form['router_path'][$key]);
      $row[] = drupal_render($form['module'][$key]);
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '6'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function menumaker_multiple_delete_confirm(&$form_state, $items) {
  $form['items'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($items as $mlid => $value) {
    $title = db_result(db_query('SELECT link_title FROM {menu_links} WHERE mlid = %d', $mlid));
    $form['items'][$mlid] = array(
      '#type' => 'hidden',
      '#value' => $mlid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) ."</li>\n",
    );
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
  $form['#submit'][] = 'menumaker_multiple_delete_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/build/menu/menumaker-bulk-edit', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function menumaker_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['items'] as $mlid => $value) {
      menu_link_delete($mlid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/build/menu/menumaker-bulk-edit';
  return;
}

